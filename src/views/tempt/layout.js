'use strict';
import StatusBarHeight from '../../components/statusBarHeight';
import React, { Component } from 'react';
import { View, Text } from 'react-native';
import styles from './style';
// import strings from './localize';

export default class Layout extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.container} >
                <StatusBarHeight />
                
            </View>
        );
    }
}
