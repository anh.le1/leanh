'use strict';

import Layout from './layout';
import { Actions } from 'react-native-router-flux';
import {
    AppState
} from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from '../../states/actions';
class Launcher extends Layout {
    constructor(props) {
        super(props);

    }
    componentWillMount() {

    }
    componentDidMount() {
        this.loadData()
            .then(() => {
                setTimeout(() => {
                    Actions.main();
                }, 1000)

                //    Actions.video();

            })
        // Actions.tempt();

    }
    async loadData() {
        this.setState({ load: 'Loading List Top Trailer...' })
        await this.props.actions.app.getTopTrailer()
        this.setState({ load: 'Loading List Top Movie...' })
        await this.props.actions.app.getTopMovie()
        this.setState({ load: 'Loading List Top Session Movie...' })
        await this.props.actions.app.getTopMovieSession()
        this.setState({ load: 'Loading List Trailer...' })
        await this.props.actions.app.getTrailer()
        this.setState({ load: 'Loading List Movie...' })
        await this.props.actions.app.getMovie()
        this.setState({ load: 'Loading List Session Movie...' })
        await this.props.actions.app.getMovieSession()
        this.setState({ load: 'Loading Success...' })

    }
}

const stateToProps = (state) => ({ ...state });
const dispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
};

export default connect(stateToProps, dispatchToProps)(Launcher);

