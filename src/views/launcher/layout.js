'use strict';

import React, {
    Component
} from 'react';

import {
    View,
    ActivityIndicator,
    Text,
    Image,
    Dimensions
} from 'react-native';

import styles from './style';
import StatusBarHeight from '../../components/statusBarHeight';
const { width, height } = Dimensions.get('window')
const LOGO = require('../../resources/splashScreen/Artwork.png');
const LOGO2 = require('../../resources/icon/f4f8eb150bb95a5698cb7ba53e05c8fd-d2q8tnu.jpg');
export default class Layout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            load: 'Loading!'
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Image source={LOGO2} style={{ height: height, position: 'absolute', opacity: 0.8 }} resizeMode='stretch' />
                {this.renderLoading()}
            </View>
        );
    }

    renderLoading() {

        return (
            <View style={{ width: width, height: height, flexDirection: 'row', alignItems: 'center' }}>
                <ActivityIndicator animating={true} size={'large'} color={'white'} style={{ marginLeft: 10 }} />
                <Text style={{ color: '#fff', fontSize: 20, fontWeight: 'bold', marginLeft: 5 }}>{this.state.load}</Text>
            </View>

        );

    }
}
