'use strict';

import {
    StyleSheet,
    Dimensions
} from 'react-native';

import Util from '../../utils';
const width = Dimensions.get('window').width;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: "#rgb(86, 70, 196)"
    },
    logoContainer: {
        flex: 1,
        flexDirection: 'row',
        paddingTop: parseInt(Util.scaleWidth(50)),
        width: width,
        paddingLeft: parseInt(Util.scaleWidth(50))
    },
    image: {
        width: parseInt(Util.scaleWidth(223)),
        height: parseInt(Util.scaleWidth(114))
    },
    footerContainer: {
        flex: 0.5,
        justifyContent: "flex-end",
        alignItems: "center",
        paddingBottom: parseInt(Util.scaleWidth(15))
    },
    studio: {
        textAlign: "center",
        color: "white",
        fontWeight: "bold",
        fontSize: 14
    },
    version: {
        textAlign: "center",
        color: "white",
        fontSize: 12
    }
});

export default styles;
