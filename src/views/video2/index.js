'use strict';

import Layout from './layout';
import { Actions } from 'react-native-router-flux';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from '../../states/actions';
import { ListView, BackAndroid } from 'react-native';
import io from 'socket.io-client/dist/socket.io.js';
const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
let a;
class video2 extends Layout {
    constructor(props) {
        super(props);
        a = this;
        this.state = {
            rate: 1,
            volume: 1,
            muted: false,
            resizeMode: 'contain',
            duration: 0.0,
            currentTime: 0.0,
            paused: false,
            flagLoad: false,
            urlVideo: 'http://www.youtube.com/api/manifest/dash/id/bf5bb2419360daf1/source/youtube?as=fmp4_audio_clear,fmp4_sd_hd_clear&sparams=ip,ipbits,expire,source,id,as&ip=0.0.0.0&ipbits=0&expire=19000000000&signature=51AF5F39AB0CEC3E5497CD9C900EBFEAECCCB5C7.8506521BFC350652163895D4C26DEE124209AA9E&key=ik0',
            dataEspisode: ds.cloneWithRows([]),
            urlruning: 'null',
            txtMessage: '',
            rowMessage: [],
            datamessage: ds.cloneWithRows([])
        }
        this.socket = new io('https://socketmovie.herokuapp.com/', { jsonp: false });
        this.socket.emit('conn', { room: this.props.url ? this.props.url : 1 })

        this.socket.on('messagerere', function (data) { //get data from server emit
            a.setState({
                rowMessage: a.state.rowMessage.concat(data),
                datamessage: ds.cloneWithRows(a.state.rowMessage.concat(data))
            })
        })

    }
    sendMessage() {
        this.socket.emit('message', {
            txtMessage: this.state.txtMessage,
            room: this.props.url ? this.props.url : 1
        })

        this.setState({ txtMessage: '' })
    }
    componentDidMount() {
        setTimeout(() => {
            this.getVideo(null)
        }, 300)
        // this.setState({
        //     urlVideo:'https://redirector.googlevideo.com/videoplayback?expire=1492597953&dur=362.974&lmt=1492397809283983&key=yt6&ip=163.172.186.251&id=o-ADPv3VV8641tvENDLHGM73HXl-J3NVW-MNNU4KmdaDND&initcwndbps=6966250&source=youtube&mn=sn-25ge7n7z&mm=31&ratebypass=yes&usequic=no&requiressl=yes&ms=au&ei=Yej2WL_RDML_cJv1vOAC&mv=m&mt=1492576286&pl=20&itag=22&mime=video%2Fmp4&gcr=fr&ipbits=0&sparams=dur%2Cei%2Cgcr%2Cid%2Cinitcwndbps%2Cip%2Cipbits%2Citag%2Clmt%2Cmime%2Cmm%2Cmn%2Cms%2Cmv%2Cpl%2Cratebypass%2Crequiressl%2Csource%2Cupn%2Cusequic%2Cexpire&signature=9D74C1F0F36FF9D0B816570669389D7AC332CAD9.C0F275F157D0850CC995406C09DA4A48E7298A04&upn=mZS7o5l2YQk',
        //     flagLoad: true
        // })
    }
    componentWillUnmount() {
        this.socket.emit('leaveRoom', this.props.url ? this.props.url : 1)
    }
    async getVideo(data) {
        if (data !== null) {
            await this.props.actions.app.geturlMovie(data);

            await this.setState({
                urlVideo: this.props.app.urlMovie.url, flagLoad: true,
                dataEspisode: ds.cloneWithRows(this.props.app.listespisode)
            })
        } else {
            if (this.props.url) {
                await this.props.actions.app.geturlMovie(this.props.url);
                await this.props.actions.app.getespisode(this.props.url)
                await this.setState({
                    urlVideo: this.props.app.urlMovie.url,
                    flagLoad: true,
                    dataEspisode: ds.cloneWithRows(this.props.app.listespisode)
                })
                // alert(Object.keys(this.props.app.listespisode).length)
                // if (Object.keys(this.props.app.listespisode).length > 0) {
                    this.setState({
                        urlruning: this.props.app.listespisode[0].href ? this.props.app.listespisode[0].href : ''
                    })
                // } else {
                //     this.setState({
                //         urlruning: this.props.app.listespisode.href ? this.props.app.listespisode.href : ''
                //     })
                // }


            } else {
                await this.setState({
                    urlVideo: 'http://www.youtube.com/api/manifest/dash/id/bf5bb2419360daf1/source/youtube?as=fmp4_audio_clear,fmp4_sd_hd_clear&sparams=ip,ipbits,expire,source,id,as&ip=0.0.0.0&ipbits=0&expire=19000000000&signature=51AF5F39AB0CEC3E5497CD9C900EBFEAECCCB5C7.8506521BFC350652163895D4C26DEE124209AA9E&key=ik0',
                    flagLoad: true
                })
            }
        }

    }
    onLoad = (data) => {
        this.setState({ duration: data.duration });

        console.log('On Load');
    };
    onProgress = (data) => {
        this.setState({ currentTime: data.currentTime });
        console.log('On process');
    };
    getCurrentTimePercentage() {
        if (this.state.currentTime > 0) {
            return parseFloat(this.state.currentTime) / parseFloat(this.state.duration);
        }
        return 0;
    };
    async getmovie(data) {

        await this.setState({
            urlVideo: 'http://www.youtube.com/api/manifest/dash/id/bf5bb2419360daf1/source/youtube?as=fmp4_audio_clear,fmp4_sd_hd_clear&sparams=ip,ipbits,expire,source,id,as&ip=0.0.0.0&ipbits=0&expire=19000000000&signature=51AF5F39AB0CEC3E5497CD9C900EBFEAECCCB5C7.8506521BFC350652163895D4C26DEE124209AA9E&key=ik0',
            flagLoad: false,
            urlruning: data
        })
        this.getVideo(data)
    }
}



const stateToProps = (state) => ({ ...state });
const dispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
};

export default connect(stateToProps, dispatchToProps)(video2);
