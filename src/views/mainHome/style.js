'use strict';

import { StyleSheet, Dimensions } from 'react-native';
const {width, height} = Dimensions.get('window');
import util from '../../utils';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#DADADA',
        
    },
    view50:{
marginBottom: util.scaleWidth(50)
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'repeat',
        padding: 5
    },
    viewHeader: {
        height: util.scaleWidth(65),
        borderRadius: 4,
        justifyContent: 'flex-end',
        alignItems: 'center',
        backgroundColor: '#deb887'
    },
    txtHeader: {
        color: '#FFFFFF',
        fontSize: util.scaleWidth(30),
        fontWeight: 'bold',
        textShadowOffset: { width: 2, height: 2 },
        textShadowRadius: 1, textShadowColor: 'black'
    },
    viewContainerMovie: {
        margin: 3,
        borderRadius: 4
    },
    viewHeaderMovie: {
        height: util.scaleWidth(30),
        opacity: 0.7,
        margin: 3,
        borderBottomLeftRadius: 4,
        borderTopRightRadius: 4,
        flexDirection: 'row'
    },
    itemMovie: {
        height: height / 4,
        width: util.scaleWidth(120),
        marginBottom: 5,
    },
    imgMovie: {
        height: height / 4,
        width: util.scaleWidth(100),
    },
    containerHeader: {
        height: util.scaleWidth(50),
        borderRadius: 4,
        backgroundColor: '#2F2B2F', opacity: 0.7,
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomLeftRadius: util.scaleWidth(65),
        borderTopRightRadius: util.scaleWidth(65),
    },
    txtTitleGroup: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#EFF856',
        marginLeft: 10
    },
    txtTitleViewMore: {
        fontSize: 14,
        fontWeight: '200',
        color: 'red',
        marginRight: 10,
        textDecorationLine: 'underline'
    }
});


export default styles;
