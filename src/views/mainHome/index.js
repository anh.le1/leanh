'use strict';

import Layout from './layout';
import { Actions } from 'react-native-router-flux';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ListView } from 'react-native';
import * as actions from '../../states/actions';
const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
class Home extends Layout {
    constructor(props) {
        super(props);
        this.state = {
            listTrailer: {},
            listMovie: {},
            listMovieSession: {},
            flag: false
        }
    }
    componentDidMount() {
        setTimeout(() => {
            this.loadDataHomePage();
        }, 500);

    }
    async loadDataHomePage() {
        await this.setState({
            listTrailer: ds.cloneWithRows(this.props.app.listTopTrailer.data),
            listMovie: ds.cloneWithRows(this.props.app.listTopMovie.data),
            listMovieSession: ds.cloneWithRows(this.props.app.listTopMovieSession.data),
            flag: true
        })
    }
    gotopage(data) {
        Actions.viewMore({ title: data });
    }
    gotoMovieDetail(id) {
        Actions.movieDetail({ urlMovie: id });
    }
}

const stateToProps = (state) => ({ ...state });
const dispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
};

export default connect(stateToProps, dispatchToProps)(Home);

