'use strict';
import StatusBarHeight from '../../components/statusBarHeight';
import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    ScrollView,
    TouchableOpacity,
    ListView, ActivityIndicator
} from 'react-native';
import styles from './style';
// import strings from './localize';


const IMG_BACKGROUND = require('../../resources/icon/backgroundTab.jpg')
const IMG_BACKGROUNDHeader = require('../../resources/icon/background.jpg')
const IMG_POSTER = require('../../resources/icon/poster.jpg')
const IMG_SOURCE = 'http://image.tmdb.org/t/p/w500';
export default class Layout extends Component {

    render() {
        return (
            <View style={styles.container} >
                <StatusBarHeight />
                {<Image style={{ position: 'absolute', top: 0, bottom: 0, left: 0, right: 0 }} source={IMG_BACKGROUND} />}
                {this.renderHeader()}

                <View style={{ height: 1, backgroundColor: '#FFFFFF', marginTop: 5 }} />
                {this.state.flag ?
                    <ScrollView style={{ marginTop: 5 }}>
                        {this.renderMovie('TRAILER MỚI', this.state.listTrailer)}
                        {this.renderMovie('PHIM LẺ HOT', this.state.listMovie)}
                        {this.renderMovie('PHIM BỘ HOT', this.state.listMovieSession)}
                        <View style={styles.view50} />
                    </ScrollView>
                    :
                    <ActivityIndicator
                        animating={this.state.animating}
                        style={[styles.centering, { height: 80 }]}
                        size="large"
                    />
                }


            </View>
        );
    }
    renderHeader() {
        return (
            <View style={styles.containerHeader}>
                <Text style={styles.txtHeader}>Hot Movie</Text>
            </View>
        );
    }
    renderMovie(Title, data) {
        return (
            <View style={styles.viewContainerMovie}>
                <View style={styles.viewHeaderMovie}>
                    <View style={{ flex: 1, alignItems: 'flex-start', justifyContent: 'center' }}><Text style={styles.txtTitleGroup}>{Title}</Text></View>
                    <TouchableOpacity style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'center' }}
                        onPress={() => this.gotopage(Title)}>
                        <Text style={styles.txtTitleViewMore}>View More ></Text>
                    </TouchableOpacity>


                </View>
                {data.getRowCount() <= 0 ?
                    <View />
                    :
                    <ListView
                        dataSource={data}
                        horizontal={true}
                        renderRow={(rowData) => this.renderItem(rowData)}
                    />
                }
            </View>
        );
    }
    renderItem(val) {
        return (

            <TouchableOpacity key={val.title} style={styles.itemMovie} onPress={() => this.gotoMovieDetail(val.url)}>
                <Image source={{ uri: val.image }} style={styles.imgMovie} resizeMode='stretch' />
            </TouchableOpacity>


        );
    }
}
