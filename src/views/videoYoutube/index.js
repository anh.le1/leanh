'use strict';

import Layout from './layout';
// import { Actions } from 'react-native-router-flux';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from '../../states/actions';

class Youtube extends Layout {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        // alert(JSON.stringify(this.props.app.listMovie))
    }
}


const stateToProps = (state) => ({ ...state });
const dispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
};

export default connect(stateToProps, dispatchToProps)(Youtube);
