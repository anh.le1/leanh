import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableOpacity
} from 'react-native';
import YouTube from 'react-native-youtube';

export default class Layout extends Component {

    state = {
        isReady: false,
        status: null,
        quality: null,
        error: null,
        isPlaying: true
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={{ padding: 10, }}>
                    <Text style={{ fontSize: 20, fontWeight: 'bold' }}>{this.props.nameVideo}</Text>
                </View>
                <YouTube
                    videoId={this.props.keyYouTube}
                    play={this.state.isPlaying}
                    hidden={false}
                    playsInline={true}
                    apiKey="AIzaSyD6u9tZ7HfVouEdWk4CuFd3BSN9URJspaU"
                    onReady={(e) => { this.setState({ isReady: true }) }}
                    onChangeState={(e) => { this.setState({ status: e.state }) }}
                    onChangeQuality={(e) => { this.setState({ quality: e.quality }) }}
                    onError={(e) => { this.setState({ error: e.error }) }}
                    style={{ alignSelf: 'stretch', height: 250, backgroundColor: 'black' }}
                />

                <TouchableOpacity onPress={() => { this.setState((s) => { return { isPlaying: !s.isPlaying }; }) }}>
                    <Text style={[styles.welcome, { color: 'blue' }]}>{this.state.status == 'playing' ? 'Pause' : 'Play'}</Text>
                </TouchableOpacity>
                <Text>{this.state.error}</Text>

            </View>
        );
    }
}

var styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
        top: 0, left: 0, right: 0, bottom: 0, position: 'absolute'
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});