'use strict';

import Layout from './layout';
import { Actions } from 'react-native-router-flux';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ListView } from 'react-native';
import * as actions from '../../states/actions';
const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
class AppHome extends Layout {
    constructor(props) {
        super(props);

        this.state = {
            row: [],
            list: ds.cloneWithRows([]),
            flagLoad: true
        };
    }
    componentDidMount() {
        setTimeout(() => {
            this.setFirstData()
            this.setState({flagLoad: false})
        },500)

    }

    async nextPage() {
        if (this.props.title === 'TRAILER MỚI') {
            let page = this.props.app.listTrailer.page;
            await this.props.actions.app.getTrailer(page + 1);
            await this.setState({
                row: this.state.row.concat(this.props.app.listTrailer.item),
                list: ds.cloneWithRows(this.state.row.concat(this.props.app.listTrailer.item))
            })
        }
        else if (this.props.title === 'PHIM LẺ HOT') {
            let page = this.props.app.listMovie.page;
            await this.props.actions.app.getMovie(page + 1)

            await this.setState({
                row: this.state.row.concat(this.props.app.listMovie.item),
                list: ds.cloneWithRows(this.state.row.concat(this.props.app.listMovie.item))
            })
        }
        else {
            let page = this.props.app.listMovieSession.page;
            await this.props.actions.app.getMovieSession(page + 1)
            await this.setState({
                row: this.state.row.concat(this.props.app.listMovieSession.item),
                list: ds.cloneWithRows(this.state.row.concat(this.props.app.listMovieSession.item)),
            })
        }
    }
    async setFirstData() {
        await this.setState({
            row:
            this.props.title === 'TRAILER MỚI'
                ?
                this.props.app.listTrailer.item
                :
                this.props.title === 'PHIM LẺ HOT'
                    ?
                    this.props.app.listMovie.item
                    :
                    this.props.app.listMovieSession.item,
            list: ds.cloneWithRows(
                this.props.title === 'TRAILER MỚI'
                    ?
                    this.props.app.listTrailer.item
                    :
                    this.props.title === 'PHIM LẺ HOT'
                        ?
                        this.props.app.listMovie.item
                        :
                        this.props.app.listMovieSession.item),
            
        })

    }
    gotoMovieDetail(id) {
        Actions.movieDetail({ urlMovie: id });
    }


}

const stateToProps = (state) => ({ ...state });
const dispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
};

export default connect(stateToProps, dispatchToProps)(AppHome);
