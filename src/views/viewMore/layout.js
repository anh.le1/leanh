'use strict';

import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import {
    View,
    Text,
    ListView,
    Image,
    ScrollView,
    TouchableOpacity,
    ActivityIndicator
} from 'react-native';
import StarRating from 'react-native-star-rating';
import styles from './style';
import strings from './localize';
import StatusBarHeight from '../../components/statusBarHeight';
const IMG_BACKGROUND = require('../../resources/icon/backgroundTab.jpg')
const IMG_BACKGROUNDHeader = require('../../resources/icon/background.jpg')
const ICON_BACK = require('../../resources/icon/arrow_backward@3x.png')

const IMG_NULL = require('../../resources/icon/noimg.png')
const IMG_SOURCE = 'http://image.tmdb.org/t/p/w500';
export default class Layout extends Component {


    rendera() {
        return (
            this.state.flagLoad ?
                this.renderLoading()
                :
                // this.rendera()
                <View />

        );
    }
    render() {
        return (
            <View style={styles.container} >
<StatusBarHeight/>
                <Image style={styles.backgroundImage} source={IMG_BACKGROUND} />
                <View style={styles.viewHeader} >

                    <TouchableOpacity style={{ flex: 1, justifyContent: 'center' }} onPress={() => Actions.pop()}>
                        <Image style={{ width: 20, height: 25, marginLeft: 10 }} source={ICON_BACK} resizeMode='contain' />
                    </TouchableOpacity>
                    <View style={{ flex: 2, alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={styles.txtHeader}>{this.props.title}</Text>
                    </View>

                    <View style={{ flex: 1 }}></View>

                </View>

                <View style={styles.viewContainerBody} />
                {this.state.flagLoad ?
                    this.renderLoading()
                    :
                    this.renderList()
                }
            </View>
        );
    }


    renderList() {
        return (
            this.state.list.getRowCount() <= 0 ?
                this.renderLoading()
                :
                <ListView
                    dataSource={this.state.list}
                    onEndReached={() => this.nextPage()}
                    initialListSize={4}
                    pageSize={4}
                    renderRow={(rowData) => this.renderMovieItems(rowData)}
                />

        );
    }
    renderLoading() {
        return (
            <View style={styles.itemLoading}>

                <ActivityIndicator
                    animating={true}
                    size="large" />
            </View>

        );
    }
    renderMovieItems(rowData) {
        return (
            <TouchableOpacity key={rowData.id} activeOpacity={0.7} style={styles.containerIteam} onPress={() => this.gotoMovieDetail(rowData.url)}>
                <View style={styles.containerImgItem}>
                    <Image source={{ uri: rowData.image }} style={styles.imgItem} resizeMode='stretch' />
                </View>
                <View style={styles.containerItemDetails}>
                    <Text style={styles.txtTitle}>{rowData.titlew}</Text>
                    <Text style={styles.txt16}>{rowData.titlev}</Text>
                </View>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={styles.txt12}> Thời Gian : {rowData.time}</Text>
                </View>
            </TouchableOpacity>
        );
    }
}