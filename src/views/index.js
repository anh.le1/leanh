'use strict';

import React, { Component } from 'react';
import { Scene, Modal, Router } from 'react-native-router-flux';
import Config from '../config';

import {
    StatusBar,
    Platform,
    View
} from 'react-native';

import TabIcon from '../components/tabIcon';

import Launcher from './launcher';


//start
import Home from './mainHome';
import New from './mainTopView';
import Search from './mainSearch';


import ViewMore from './viewMore';
import MovieDetail from './movieDetails';

import Video from './video';
import Video2 from './video2';
// import Video2 from './video2';
import Youtube from './videoYoutube';
// import MovieDetail from './movieDetails';
// import ViewMore from './viewMore';
import Tempt from './tempt';


export default class extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Router>
                <Scene key="modal" component={Modal} >
                    <Scene key="root" hideNavBar={true} >

                        <Scene key="main" tabs={true} duration={1} panHandlers={null}
                            tabBarStyle={{
                                borderTopWidth: 1,
                                borderTopColor: Config.TABBAR_BORDER_COLOR,
                                backgroundColor: Config.TABBAR_COLOR
                            }}>
                            <Scene key="home" component={Home} icon={TabIcon} hideNavBar={true} panHandlers={null}  initial={true}/>
                            <Scene key="search" component={Search} icon={TabIcon} hideNavBar={true} panHandlers={null} />
                            <Scene key="view" component={New} icon={TabIcon} hideNavBar={true} panHandlers={null}  />
                            
                        </Scene>

                        <Scene key="launcher" component={Launcher} initial={true} />
                        <Scene key="viewMore" component={ViewMore} />
                        <Scene key="movieDetail" component={MovieDetail} />
                        <Scene key="tempt" component={Tempt} />
                    </Scene>
                    {/*<Scene key="searchAdvand" component={SearchAdvand} />*/}
                    <Scene key="video" component={Video} />
                    <Scene key="video2" component={Video2} />
                    {/*<Scene key="video2" component={Video2} />*/}
                    <Scene key="youtube" component={Youtube} />

                </Scene>
            </Router>
        );
    }
}
