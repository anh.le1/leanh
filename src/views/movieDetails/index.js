'use strict';

import Layout from './layout';
import { Actions } from 'react-native-router-flux';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from '../../states/actions';
import { ListView } from 'react-native';
const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
class MovieDetail extends Layout {
    constructor(props) {
        super(props);
        this.state = {
            dataMovie: {},
            listData: {},
            flag: false
        }
    }
    componentDidMount() {

        setTimeout(() => {
            this.loadData()
        }, 300)
    }
    async loadData() {
        await this.props.actions.app.getMovieDetails(this.props.urlMovie);
        var dataMovie = this.props.app.movieDetails;
        await this.setState({
            dataMovie: dataMovie,
            listData: ds.cloneWithRows(dataMovie.actor),
            flag: true
        });

    }
}

const stateToProps = (state) => ({ ...state });
const dispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
};


export default connect(stateToProps, dispatchToProps)(MovieDetail);
