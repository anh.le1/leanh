'use strict';

import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import {
    View,
    Text,
    ListView,
    Image,
    ScrollView,
    TouchableOpacity,
    ActivityIndicator
} from 'react-native';
import PopupDialog, { DialogTitle } from 'react-native-popup-dialog';
import StatusBarHeight from '../../components/statusBarHeight';
import styles from './style';
import strings from './localize';


const IMG_BACKGROUNDHeader = require('../../resources/icon/background.jpg')
const ICON_BACK = require('../../resources/icon/arrow_backward@3x.png')
const IMG_BACKGROUND = require('../../resources/icon/backgroundTab.jpg')
const IMG_NULL = require('../../resources/icon/noimg.png')
const IMG_SOURCE = 'http://image.tmdb.org/t/p/w500';
export default class Layout extends Component {


    /*render() {
        return (
            <View>
                <Text>xin chao</Text>
            </View>
        );
    }*/
    render() {
        return (
            <View style={styles.container} >
                <StatusBarHeight />
                {<Image style={{ position: 'absolute', top: 0, bottom: 0, left: 0, right: 0 }} source={IMG_BACKGROUND} />}
                <View style={styles.viewHeader} >

                    <TouchableOpacity style={{ flex: 1, justifyContent: 'center' }} onPress={() => this.state.flagLoad ? Actions.pop() : Actions.pop()}>
                        <Image style={{ width: 20, height: 25, marginLeft: 10 }} source={ICON_BACK} resizeMode='contain' />
                    </TouchableOpacity>
                    <View style={{ flex: 2, alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={styles.txtHeader}>{'Movie Details'}</Text>
                    </View>

                    <View style={{ flex: 1 }}></View>

                </View>

                <View style={styles.viewContainerBody} />
                {this.state.flag ?
                    this.renderDatabody()
                    :
                    <ActivityIndicator
                        animating={this.state.animating}
                        style={[styles.centering, { height: 80 }]}
                        size="large"
                    />
                }


            </View >
        );
    }
    renderDatabody() {
        return (
            <ScrollView style={styles.viewBody} key={1} scrollEventThrottle={200}
                         automaticallyAdjustContentInsets={false}
                          onResponderMove={()=>{console.warn('outer responding');}}>
                <View style={styles.viewDes}>
                    <View style={{ padding: 5 }}>
                        <Image source={{ uri: this.state.dataMovie.detail.image }} style={styles.styleIMG} resizeMode='stretch' />
                    </View>
                    <View style={{ flex: 1, marginRight: 10 }}>
                        <ScrollView
                         style={styles.viewDetails} key={2}
                         scrollEventThrottle={200}
                         automaticallyAdjustContentInsets={false}
                          onResponderMove={()=>{console.warn('outer responding');}}
                         >
                            <View style={{ flex: 1 }}>
                                <Text style={styles.nameMovie}>{this.state.dataMovie.detail.title1}</Text>
                                <Text style={styles.nameMovie2}>{this.state.dataMovie.detail.title2}</Text>
                                {this.renderText('Status :', this.state.dataMovie.descrip.Detail.status ? this.state.dataMovie.descrip.Detail.status : 'N/A')}
                                {this.renderText('ibm:', this.state.dataMovie.descrip.Detail.ibm ? this.state.dataMovie.descrip.Detail.ibm : 'N/A')}
                                {this.renderText('voteIBM :', this.state.dataMovie.descrip.Detail.voteIBM ? this.state.dataMovie.descrip.Detail.voteIBM : 'N/A')}
                                {this.renderText('director :', this.state.dataMovie.descrip.Detail.director ? this.state.dataMovie.descrip.Detail.director : 'N/A')}
                                {this.renderText('state:', this.state.dataMovie.descrip.Detail.state ? this.state.dataMovie.descrip.Detail.state : 'N/A')}
                                {this.renderText('year :', this.state.dataMovie.descrip.Detail.year ? this.state.dataMovie.descrip.Detail.year : 'N/A')}
                                {this.renderText('dateRelease :', this.state.dataMovie.descrip.Detail.dateRelease ? this.state.dataMovie.descrip.Detail.dateRelease : 'N/A')}
                                {this.renderText('time :', this.state.dataMovie.descrip.Detail.time ? this.state.dataMovie.descrip.Detail.time : 'N/A')}
                                {this.renderText('quality:', this.state.dataMovie.descrip.Detail.quality ? this.state.dataMovie.descrip.Detail.quality : 'N/A')}
                                {this.renderText('language :', this.state.dataMovie.descrip.Detail.language ? this.state.dataMovie.descrip.Detail.language : 'N/A')}
                                {this.renderText('pixcel :', this.state.dataMovie.descrip.Detail.pixcel ? this.state.dataMovie.descrip.Detail.pixcel : 'N/A')}
                                {this.renderText('typeMovie :', this.state.dataMovie.descrip.Detail.typeMovie ? this.state.dataMovie.descrip.Detail.typeMovie : 'N/A')}
                                {this.renderText('company :', this.state.dataMovie.descrip.Detail.company ? this.state.dataMovie.descrip.Detail.company : 'N/A')}
                                {this.renderText('view :', this.state.dataMovie.descrip.Detail.view ? this.state.dataMovie.descrip.Detail.view : 'N/A')}

                            </View>
                        </ScrollView>
                    </View>



                </View>
                <View style={styles.viewTitle}>
                    <Text style={styles.txtTitle}>Descriptions</Text>
                    <View style={styles.groupBTN}>
                        {this.state.dataMovie.detail.haveVideo === "yes" ?
                            <TouchableOpacity style={styles.btnWatch} onPress={() => Actions.video({ url: this.props.urlMovie })}>
                                <Text style={styles.txtBtn}>Watch Movie</Text>
                            </TouchableOpacity>
                            :
                            <View />
                        }
                        <TouchableOpacity style={styles.btnTrailer}
                            onPress={() => Actions.youtube({ nameVideo: this.state.dataMovie.detail.trailername, keyYouTube: this.state.dataMovie.detail.idYoutube })}>
                            <Text style={styles.txtBtn}>Trailer</Text>
                        </TouchableOpacity>


                    </View>
                </View>
                <ScrollView style={styles.scrollViewDes}>
                    <Text style={{ color: '#FFFFFF', fontSize: 14, marginLeft: 5 }}>
                        {this.state.dataMovie.detail.description}
                    </Text>

                </ScrollView>

                {/*<View style={styles.viewTitle}>
                        <Text style={styles.txtTitle}>Comment</Text>
                    </View>
                    {this.state.listComment.getRowCount() > 0 ?
                        < ListView
                            dataSource={this.state.listComment}
                            enableEmptySections={true}
                            renderRow={(rowData) => this.renderComment(rowData)}
                        />
                        : this.state.flagLoad ?
                            this.renderLoading()
                            :
                            <Text style={{ color: '#FFFFFF', fontSize: 14, marginLeft: 5 }}> No Comment</Text>
                    }*/}
                <View style={styles.viewTitle}>
                    <Text style={styles.txtTitle}>Movies Actor</Text>
                </View>
                <View style={[styles.viewContainerMovie]}>
                    {/*{alert(this.state.listData.getRowCount())}*/}
                    {this.state.listData.getRowCount() <= 0 ?
                        this.renderLoading()
                        :
                        < ListView
                            dataSource={this.state.listData}
                            horizontal={true}
                            enableEmptySections={true}
                            renderRow={(rowData) => this.renderItem(rowData)}
                        />
                    }
                </View>
            </ScrollView>
        );
    }
    renderText(a, b) {
        return (
            <View style={styles.textstatus}>
                <View>
                    <Text style={styles.txtBold16}>{a}</Text>
                </View>
                <View style={{flex:1}}>
                    <Text style={styles.txt16}>{b}</Text>
                </View>


            </View>
        );
    }
    renderComment(rowData) {
        return (
            <View key={rowData.id} style={{ flexDirection: 'row', borderBottomColor: '#FFFFFF', borderBottomWidth: 1 }}>
                <Image source={IMG_BACKGROUNDHeader} style={styles.imgComment} resizeMode='stretch' />
                <View style={styles.viewComment}>
                    <Text style={styles.txtBold16}>{rowData.author} </Text>
                    <Text style={styles.txt16}>{rowData.content}</Text>
                </View>
            </View>
        );
    }

    renderItem(val) {
        return (
            <TouchableOpacity key={val.id} style={styles.itemMovie}>
                <Image source={val.poster_path === null ? IMG_NULL : { uri: val.img }} style={styles.imgMovie} resizeMode='stretch' />
                <View style={{ flex: 1, justifyContent: 'flex-end', padding: 4 }}>
                    <Text style={styles.txtBold16}>{val.name}</Text>
                    <Text style={styles.txt16}>{val.movieName}</Text>
                </View>
            </TouchableOpacity>
        );
    }
    renderLoading() {
        return (
            <View style={styles.itemLoading}>

                <ActivityIndicator
                    animating={true}
                    size="large" />
            </View>

        );
    }
    renderItemPopup(val, index) {
        return (
            <TouchableOpacity key={val.id} style={{ backgroundColor: 'gray', padding: 10, margin: 5 }} onPress={() => Actions.youtube({ keyYouTube: val.key, nameVideo: val.name })}>
                <Text style={{ fontSize: 18, color: '#FFFFFF', fontWeight: 'bold' }}>{val.name}</Text>
            </TouchableOpacity>
        );
    }
}