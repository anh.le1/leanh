'use strict';

import { StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');
import util from '../../utils';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#DADADA'
    },
    backgroundImage: { position: 'absolute', top: 0, bottom: 0, left: 0, right: 0, height: height, backgroundColor: 'black', opacity: 0.6 },
    viewHeader: {
        height: util.scaleWidth(50),
        borderRadius: 4,
        backgroundColor: 'transparent',
        flexDirection: 'row',
        opacity: 0.6
    },
    txtHeader: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#FFFFFF'
    },
    viewContainerMovie: {
        margin: 3,
        borderRadius: 4
    },
    textstatus: {
        flex:1,
        marginLeft: 5,
        flexDirection: 'row',
        
    },
    viewHeaderMovie: {
        height: util.scaleWidth(30),
        backgroundColor: 'black',
        opacity: 0.7,
        margin: 3,
        borderBottomLeftRadius: 4,
        borderTopRightRadius: 4,
        alignItems: 'center',
        justifyContent: 'center'
    },
    itemMovie: {
        height: height / 4,
        width: util.scaleWidth(120),
        marginBottom: util.scaleWidth(10),
        marginLeft: 5, marginRight: 5
    },
    imgMovie: {
        height: height / 4,
        width: util.scaleWidth(120),
        backgroundColor: 'gray',
        position: 'absolute'
    },
    IMGPoster: {
        height: util.scaleWidth(65),
        alignItems: 'center',
        justifyContent: 'flex-end',
        flexDirection: 'row'
    },
    viewContainerBody: {
        height: 1,
        backgroundColor: '#FFFFFF',
        marginTop: 5,
        opacity: 0.6
    },
    viewBody: {
        flex: 1,
        padding: 5
    },
    viewDes: {
        flex: 1,
        borderRadius: 4,
        flexDirection: 'row'
    },
    viewDetails: {
        height: util.scaleWidth(180)
    },
    nameMovie: {
        color: '#daa520',
        fontSize: 22,
        fontWeight: 'bold',
        marginLeft: 5
    },
    nameMovie2: {
        color: 'gray',
        fontSize: 16,
        fontWeight: '200',
        marginLeft: 5
    },
    styleIMG: {
        width: util.scaleWidth(130),
        height: util.scaleWidth(180)
    },
    viewTitle: {
        padding: util.scaleWidth(10),
        borderBottomWidth: 1,
        borderBottomColor: '#FFFFFF',
        flexDirection: 'row'
    },
    txtTitle: {
        color: '#00008b',
        fontSize: 18,
        fontWeight: 'bold',
        marginLeft: 5
    },
    groupBTN: {
        flex: 1,
        justifyContent: 'flex-end',
        flexDirection: "row"
    },
    btnWatch: {
        width: util.scaleWidth(100),
        backgroundColor: '#4b0082',
        marginRight: 5,
        justifyContent: 'center',
        alignItems: 'center', borderRadius: 4
    },
    btnTrailer: {
        width: util.scaleWidth(100),
        backgroundColor: '#191970',
        marginRight: 5,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4
    },
    txtBtn: {
        color: '#FFFFFF',
        fontSize: 16,
        fontWeight: 'bold'
    },
    scrollViewDes: {
        backgroundColor: 'transparent',
        maxHeight: util.scaleWidth(200),
        marginTop: 5
    },
    scrollViewComment: {
        maxHeight: util.scaleWidth(200),
        marginTop: 5,
    },
    txtBold16: {
        color: '#FFFFFF',
        fontSize: 16,
        fontWeight: 'bold'
    },
    txt16: {
        color: '#FFFFFF',
        fontSize: 16, marginLeft: 8
    },
    imgComment: {
        width: util.scaleWidth(35),
        height: util.scaleWidth(35),
        margin: util.scaleWidth(10)
    },
    itemLoading: {
        alignItems: 'center', justifyContent: 'center', height: util.scaleWidth(80), width: width
    },
    viewComment: {
        padding: 5,
        width: width - util.scaleWidth(60),
    }
});


export default styles;
