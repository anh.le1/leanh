'use strict';

import { StyleSheet, Dimensions } from 'react-native';
const {width, height } = Dimensions.get('window');
import Util from '../../utils';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#DADADA',
    },
    containViewSearch: {
        height: Util.scaleWidth(40),
        alignItems: 'center',
        justifyContent: 'center'
    },
    viewSearch: {
        width: width * 80 / 100,
        height: Util.scaleWidth(40),
        borderRadius: Util.scaleWidth(30),
        flexDirection: 'row',
        backgroundColor: '#303137'
    },
    txtsearh: {
        color: '#FFFFFF',
        marginLeft: Util.scaleWidth(10)
    },
    imgSearch: {
        width: Util.scaleWidth(25),
        height: Util.scaleWidth(25)
    },
    caItems: {
        padding: 5,
        marginBottom: 50
    },
    viewNoItem: {
        marginTop: 2,
        alignItems: 'center'
    },
    txtNoItem: {
        margin: Util.scaleWidth(10),
        color: '#FFFFFF',
        fontSize: 16,
        fontWeight: 'bold'
    },
    loading: {
        width: Util.scaleWidth(80),
        height: Util.scaleWidth(80)
    },
    containerHeader: {
        height: Util.scaleWidth(50),
        borderRadius: 4,
        backgroundColor: '#2F2B2F', opacity: 0.7,
        justifyContent: 'flex-end',
        alignItems: 'center',
        borderBottomLeftRadius: Util.scaleWidth(65),
        borderTopRightRadius: Util.scaleWidth(65)
    },
    txtHeader: {
        color: '#FFFFFF',
        fontSize: 30,
        fontWeight: 'bold',
        textShadowOffset: { width: 2, height: 2 },
        textShadowRadius: 1, textShadowColor: '#1C1762'
    },
    viewItems: {
        backgroundColor: '#191919',
        flexDirection: 'row',
        marginTop: 2,
        opacity: 0.7
    },
    viewIMG: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: Util.scaleWidth(10)
    },
    imgItems: {
        width: Util.scaleWidth(70),
        height: Util.scaleWidth(100),
        marginLeft: 5
    },
    viewItemDetails: {
        flex: 4,
        padding: Util.scaleWidth(10)
    },
    txt16Title: {
        fontSize: 16,
        color: '#FFFFFF'
    },
    txt16Text: {
        color: '#665A3F',
        fontSize: 16
    },
    txt12Text: {
        color: '#665A3F',
        fontSize: 12
    }
});

export default styles;