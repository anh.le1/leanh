'use strict';
import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    Dimensions,
    TouchableOpacity,
    TextInput,
    ListView,
    ActivityIndicator
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from './style';
import Icon from 'react-native-vector-icons/MaterialIcons';
import StatusBarHeight from '../../components/statusBarHeight';

const IMG_POSTER = require('../../resources/icon/poster.jpg')
const IMG_BACKGROUND = require('../../resources/icon/backgroundTab.jpg')
const ICON_SEARCH = require('../../resources/icon/search_icon_white@3x.png')
const ICON_ERROR = require('../../resources/icon/noimg.png')
const width = Dimensions.get('window').width;
const IMG_SOURCE = 'http://image.tmdb.org/t/p/w500';
export default class Layout extends Component {
    render() {
        return (
            <View style={styles.container} >
                {<Image style={{ position: 'absolute', top: 0, bottom: 0, left: 0, right: 0 }} source={IMG_BACKGROUND} />}
                <StatusBarHeight />
                {this.renderHeader()}
                <View style={{ height: 1, backgroundColor: '#FFFFFF', marginTop: 5 }} />

            </View >
        );
    }
    renderHeader() {
        return (
            <View style={styles.containerHeader}>
                <Text style={styles.txtHeader}>Setting</Text>
            </View>
        );
    }


}