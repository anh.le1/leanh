'use strict';

import Layout from './layout';
import { Actions } from 'react-native-router-flux';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ListView } from 'react-native';
import * as actions from '../../states/actions';
const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
class AppSearch extends Layout {
    constructor(props) {
        super(props);
        this.state = {
            txtsearch: '',
            flagSearch: false,
            listSearch: ds.cloneWithRows([]),
            ht: 'null', tl: 'null', qg: 'null', year: 'null'
        }
    }

    async onSearch() {
        if (this.state.txtsearch === '') {
            alert('Chưa nhập gì sao mà tìm!@@');
        } else {
            await this.setState({
                rowSearch: [],
                listSearch: ds.cloneWithRows([]),
                flagSearch: true
            })

            this.setState({ flagSearch: true })
            await this.props.actions.app.getsearchtext(this.state.txtsearch);
            await this.setState({
                listSearch: ds.cloneWithRows(this.props.app.listSearch.item),
            })
            this.setState({ flagSearch: false })
        }
    }

    async onSearchADV() {
        await this.setState({
            rowSearch: [],
            listSearch: ds.cloneWithRows([]),
            flagSearch: true
        })

        this.setState({ flagSearch: true })
        await this.props.actions.app.getsearchadv(this.state.ht, this.state.tl, this.state.qg, this.state.year);
        await this.setState({
            listSearch: ds.cloneWithRows(this.props.app.listSearch.item),
        })
        this.setState({ flagSearch: false })
    }
    gotoMovieDetail(id) {
        Actions.movieDetail({ urlMovie: id });
    }
}

const stateToProps = (state) => ({ ...state });
const dispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
};

export default connect(stateToProps, dispatchToProps)(AppSearch);
