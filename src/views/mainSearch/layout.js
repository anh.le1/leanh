'use strict';
import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    Dimensions,
    TouchableOpacity,
    TextInput,
    ListView,
    ActivityIndicator,
    Picker, ScrollView
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from './style';
import Icon from 'react-native-vector-icons/MaterialIcons';
import StatusBarHeight from '../../components/statusBarHeight';
import PopupDialog, { DialogTitle, SlideAnimation } from 'react-native-popup-dialog';

const IMG_POSTER = require('../../resources/icon/poster.jpg')
const IMG_BACKGROUND = require('../../resources/icon/backgroundTab.jpg')
const ICON_SEARCH = require('../../resources/icon/search_icon_white@3x.png')
const ICON_ERROR = require('../../resources/icon/noimg.png')
const width = Dimensions.get('window').width;
const IMG_SOURCE = 'http://image.tmdb.org/t/p/w500';
export default class Layout extends Component {
    render() {
        return (
            <View style={styles.container} >
                {<Image style={{ position: 'absolute', top: 0, bottom: 0, left: 0, right: 0 }} source={IMG_BACKGROUND} />}

                <StatusBarHeight />
                {this.renderHeader()}
                <View style={{ height: 1, backgroundColor: '#FFFFFF', marginTop: 5 }} />
                <View style={{ height: 1, marginTop: 5 }} />
                <View style={styles.containViewSearch}>
                    <View style={styles.viewSearch}>
                        <View style={{ flex: 1 }}>
                            <TextInput
                                style={styles.txtsearh}
                                placeholder={'SEARCH!...'}
                                placeholderTextColor="gray"
                                onChangeText={(val) => this.setState({ txtsearch: val })}
                                underlineColorAndroid={'transparent'} />
                        </View>
                        <TouchableOpacity style={{ flex: 0.2, justifyContent: 'center', alignItems: 'center' }} onPress={() => this.onSearch()} >
                            <Image source={ICON_SEARCH} style={styles.imgSearch} resizeMode='stretch' />
                        </TouchableOpacity>


                    </View>
                    <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center', marginLeft: 5 }} onPress={() => this.popupDialog.show()} >
                        <Icon name="zoom-in" size={30} color="#900" />
                    </TouchableOpacity>

                </View>
                <View style={styles.caItems}>
                    {this.state.listSearch.getRowCount() <= 0 ?

                        !this.state.flagSearch ?
                            <View style={styles.viewNoItem}>
                                <Text style={styles.txtNoItem}> Input Text and Click search!</Text>
                            </View>

                            :
                            <View style={styles.viewNoItem}>
                                <ActivityIndicator style={styles.loading} />
                            </View>

                        :
                        <View>
                            <ListView
                                dataSource={this.state.listSearch}
                                renderRow={(rowData) => this.renderMovieItems(rowData)}
                            />
                            <View style={{ height: 115 }} />
                        </View>

                    }
                </View>
                {this.renderPop()}

            </View >
        );
    }
    renderHeader() {
        return (
            <View style={styles.containerHeader}>
                <Text style={styles.txtHeader}>Search</Text>
            </View>
        );
    }
    renderMovieItems(rowData) {
        return (
            <TouchableOpacity key={rowData.url} style={styles.viewItems} onPress={() => this.gotoMovieDetail(rowData.url)}>
                <View style={styles.viewIMG}>
                    <Image source={rowData.image != null ? { uri: rowData.image } : ICON_ERROR} style={styles.imgItems} resizeMode='stretch' />
                </View>
                <View style={styles.viewItemDetails}>
                    <Text style={styles.txt16Title}>{rowData.titlev}</Text>
                    <Text style={styles.txt16Title}>{rowData.titlew}</Text>
                </View>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={styles.txt12Text}>{rowData.time}</Text>
                </View>
            </TouchableOpacity>
        );
    }
    renderPop() {
        return (
            <PopupDialog
                dialogTitle={<DialogTitle title="Advan Search" />}
                ref={(popupDialog) => { this.popupDialog = popupDialog; }}
                style={styles.fullscreenCenter}
            >
                <ScrollView style={{ padding: 10, backgroundColor: '#a9a9a9' }}>


                    <Picker
                        selectedValue={this.state.ht}
                        onValueChange={(lang) => this.setState({ ht: lang })}>
                        <Picker.Item label="Hình thức" value="null" />
                        <Picker.Item label="Phim lẻ" value="phim-le" />
                        <Picker.Item label="phim bộ" value="phim-bo" />
                        <Picker.Item label="Phim chiếu rạp" value="phim-chieu-rap" />
                        <Picker.Item label="Phim kinh điển" value="phim-kinh-dien" />
                    </Picker>

                    <Picker
                        selectedValue={this.state.tl}
                        onValueChange={(lang) => this.setState({ tl: lang })}>
                        <Picker.Item label="Thể loại" value="null" />
                        <Picker.Item label="Phim hành động" value="phim-hanh-dong" />
                        <Picker.Item label="Phim viễn tưởng" value="phim-vien-tuong" />
                        <Picker.Item label="Phim chiến tranh" value="phim-chien-tranh" />
                        <Picker.Item label="Phim hình sự" value="phim-hinh-su" />
                        <Picker.Item label="Phim phiêu lưu" value="phim-phieu-luu" />
                        <Picker.Item label="Phim hài hước" value="phim-hai-huoc" />
                        <Picker.Item label="Phim võ thuật" value="phim-vo-thuat" />
                        <Picker.Item label="Phim kinh dị" value="phim-kinh-di" />
                        <Picker.Item label="Phim hồi hộp-Gây cấn" value="phim-hoi-hop-gay-can" />
                        <Picker.Item label="Phim Bí ẩn-Siêu nhiên" value="phim-bi-an-sieu-nhien" />
                        <Picker.Item label="Phim cổ trang" value="phim-co-trang" />
                        <Picker.Item label="Phim thần thoại" value="phim-than-thoai" />
                        <Picker.Item label="Phim tâm lý" value="phim-tam-ly" />
                        <Picker.Item label="Phim tài liệu" value="phim-tai-lieu" />
                        <Picker.Item label="Phim tình cảm-Lãng mạn" value="phim-tinh-cam-lang-man" />
                        <Picker.Item label="Phim chính kịch - Drama" value="phim-chinh-kich-drama" />
                        <Picker.Item label="Phim Thể thao-Âm nhạc" value="phim-the-thao-am-nhac" />
                        <Picker.Item label="Phim gia đình" value="phim-gia-dinh" />
                        <Picker.Item label="Phim hoạt hình" value="phim-hoat-hinh" />
                    </Picker>

                    <Picker
                        selectedValue={this.state.qg}
                        onValueChange={(lang) => this.setState({ qg: lang })}>
                        <Picker.Item label="Quốc gia" value="null" />
                        <Picker.Item label="Việt Nam" value="vn" />
                        <Picker.Item label="Trung Quốc" value="cn" />
                        <Picker.Item label="Mỹ" value="us" />
                        <Picker.Item label="Hàn Quốc" value="kr" />
                        <Picker.Item label="Nhật Bản" value="jp" />
                        <Picker.Item label="Hồng Kông" value="hk" />
                        <Picker.Item label="Ấn Độ" value="in" />
                        <Picker.Item label="Thái Lan" value="th" />
                        <Picker.Item label="Pháp" value="fr" />
                        <Picker.Item label="Đài Loan" value="tw" />
                        <Picker.Item label="Úc" value="au" />
                        <Picker.Item label="Anh" value="uk" />
                        <Picker.Item label="Canada" value="ca" />
                    </Picker>

                    <Picker
                        selectedValue={this.state.year}
                        onValueChange={(lang) => this.setState({ year: lang })}>
                        <Picker.Item label="Năm" value="null" />
                        <Picker.Item label="2017" value="2017" />
                        <Picker.Item label="2016" value="2016" />
                        <Picker.Item label="2015" value="2015" />
                        <Picker.Item label="2014" value="2014" />
                        <Picker.Item label="-2013" value="Trước 2013" />
                    </Picker>

                    <TouchableOpacity
                        style={{ marginTop: 5, padding: 5, borderRadius: 4, backgroundColor: '#b8860b', right: 20, left: 5}}
                        onPress={() => { this.onSearchADV() }}
                        disabled={this.state.flagSearch ? true : false}
                    >

                        {this.state.flagSearch ?
                            <View style={{flexDirection: 'row' }}>
                                
                                <Text style={styles.txt16Text}>Loading...</Text>
                                <ActivityIndicator size={'small'}/>
                            </View>

                            :
                            <Text style={styles.txt16Text}>Tìm Kiếm</Text>}
                    </TouchableOpacity>

                </ScrollView>
            </PopupDialog>
        );
    }
}