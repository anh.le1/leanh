'use strict';

import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    Image,
    ListView,
    ActivityIndicator,
    Dimensions,
    TextInput,
    BackAndroid,
    Slider,
    ScrollView
} from 'react-native';
import Video from 'react-native-video';
import Slider2 from 'react-native-slider';
import { PagerTabIndicator, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator } from 'rn-viewpager';
import styles from './style';
import { Actions } from 'react-native-router-flux';
import { AutoGrowingTextInput } from 'react-native-autogrow-textinput';
// import strings from './localize';
const { width, height } = Dimensions.get('window');

const vlum = require('../../resources/icon/volume-clipart-audio-volume-low-panel.png')
const ICON_PLAY = require('../../resources/icon/played.png')
const ICON_BACK = require('../../resources/icon/arrow_backward@3x.png')
const ICON_PAUSE = require('../../resources/icon/paused2.png')
const ICON_RESIZE = require('../../resources/icon/resize-icon-27798.png')

let timeloading = 30;

export default class Layout extends Component {

    video: Video;


    ComponentDidMount() {
        // setInterval(() => { this.timeloading -= this.timeloading; }, 1000)

    }
    render() {
        this.getCurrentTimePercentage();
        return (
            <View style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, backgroundColor: 'black' }}>

                {this.state.flagLoad ? this.renderVideo() : this.renderLoad()}
                <IndicatorViewPager
                    initialPage={1}
                    style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0 }}
                >
                    <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                        {this.renderChat()}

                    </View>
                    <View>
                        {this.renderControler()}
                    </View>

                    <View>
                        <View style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, backgroundColor: 'gray', opacity: 0.4 }}></View>
                        {this.state.dataEspisode.getRowCount() <= 0 ?
                            this.state.flagLoad ?
                                <View style={styles.fullScreenCenter}><Text >Không có tập khác hay server khác!!!</Text></View>

                                :
                                this.renderLoad()
                            :
                            <ListView
                                dataSource={this.state.dataEspisode}
                                removeClippedSubviews={false}
                                renderRow={(data) => this.renderEspisode(data)}
                            />

                        }

                    </View>
                </IndicatorViewPager>


            </View >
        );
    }
    renderLoad() {
        return (
            <View style={{
                position: 'absolute',
                top: 0, bottom: 0, left: 0, right: 0,
                justifyContent: 'center', alignItems: 'center'
            }}>
                <ActivityIndicator style={{ width: 100, height: 100 }} />

            </View>
        );
    }
    renderVideo() {
        return (
            <View style={{
                position: 'absolute',
                top: 0, bottom: 0, left: 0, right: 0,
                justifyContent: 'center', alignItems: 'center'
            }} >
                <Video
                    ref={(ref: Video) => { this.video = ref }}
                    /* For ExoPlayer */
                    /*source={{
                        uri: 'http://www.youtube.com/api/manifest/dash/id/bf5bb2419360daf1/source/youtube?as=fmp4_audio_clear,fmp4_sd_hd_clear&sparams=ip,ipbits,expire,source,id,as&ip=0.0.0.0&ipbits=0&expire=19000000000&signature=51AF5F39AB0CEC3E5497CD9C900EBFEAECCCB5C7.8506521BFC350652163895D4C26DEE124209AA9E&key=ik0',
                        type: 'mpd'
                    }}*/
                    /* source={{
                         uri: 'https://r1---sn-i3b7kn76.googlevideo.com/videoplayback?expire=1492597953&dur=362.974&lmt=1492397809283983&key=cms1&ip=14.161.31.62&id=o-ADPv3VV8641tvENDLHGM73HXl-J3NVW-MNNU4KmdaDND&source=youtube&ratebypass=yes&requiressl=yes&ei=Yej2WL_RDML_cJv1vOAC&pl=24&itag=22&mime=video%2Fmp4&gcr=fr&ipbits=0&sparams=dur,ei,expire,gcr,id,initcwndbps,ip,ipbits,itag,lmt,mime,mm,mn,ms,mv,pl,ratebypass,requiressl,source,upn,usequic&signature=691629C6356774A575AA8985329931682DE0C41B.1C1615D96859061D56A4986AEFFED19CDA2F409F&upn=mZS7o5l2YQk&cms_redirect=yes&mm=31&mn=sn-i3b7kn76&ms=au&mt=1492576286&mv=m'
                     }}*/
                    source={{ uri: this.state.urlVideo }}
                    style={styles.fullScreen}
                    rate={this.state.rate}
                    paused={this.state.paused}
                    volume={this.state.volume}
                    muted={this.state.muted}
                    resizeMode={this.state.resizeMode}
                    onLoadStart={() => console.log('start')}
                    onLoad={this.onLoad}
                    onProgress={this.onProgress}
                    onEnd={this.onEnd}
                    onError={() => console.log('Error')}
                    onAudioBecomingNoisy={() => console.log('audio')}
                    onAudioFocusChanged={() => console.log('audio focus')}
                    repeat={false}

                />
            </View>
        );
    }
    renderControler() {
        return (
            <View style={{ position: 'absolute', left: 20, right: 20, bottom: 20, top: 20 }}>
                <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                    {this.state.paused ?
                        <View style={styles.fullScreenCenter}>
                            <Image source={ICON_PAUSE} style={{ width: 50, height: 50 }} resizeMode='stretch' />
                        </View>
                        : <View />}

                    <TouchableOpacity style={{ flex: 1 }} onPress={() => { this.setState({ paused: !this.state.paused }) }} />
                    <View style={{ height: 100 }}>
                        {this.renderMenu()}
                        <View style={{ flexDirection: 'row' }}>
                            {this.renderSlider2Time()}
                        </View>
                        {/*<View style={{ marginLeft: 5 }}>
                            {this.renderVolume()}
                        </View>*/}
                    </View>
                </View>


            </View>
        );
    }
    renderVolume() {
        return (
            <View style={{ flex: 1, alignItems: 'flex-start', flexDirection: 'row' }}>
                <Image source={vlum} style={{ width: 20, height: 20, marginLeft: 10 }} />
                <View style={{ height: 20, width: 100 }}>
                    <Slider
                        minimumTrackTintColor='#31a4db'
                        thumbTouchSize={{ width: 50, height: 40 }}
                        value={this.state.volume}
                        maximumValue={1}
                        onValueChange={(val) => this.setState({ volume: val })}
                    />

                </View >
                <Text style={styles.controlOption}>{(this.state.volume * 100).toFixed(0)}%</Text>

            </View>
        );
    }
    renderSlider2Time() {
        return (
            <View style={{ flex: 1, alignItems: 'center', flexDirection: 'row' }}>
                <Text style={{ color: '#d8bfd8', fontWeight: 'bold', justifyContent: 'center', alignItems: 'center' }}>
                    {Math.floor((this.state.currentTime) / 60).toFixed(0) + ':' + (this.state.currentTime % 60).toFixed(0)}
                </Text>
                <View style={{flex:1 }}>
                    <Slider
                        maximumValue={this.state.duration}
                        onValueChange={(val) => this.video.seek(val)}
                        value={this.state.currentTime}
                        minimumTrackTintColor='#31a4db'
                        maximumTrackTintColor={'red'}
                        thumbTintColor={'red'}
                    />
                </View >
                <Text style={{ color: '#d8bfd8', fontWeight: 'bold', justifyContent: 'center', alignItems: 'center' }}>
                    {Math.floor((this.state.duration) / 60).toFixed(0) + ':' + (this.state.duration % 60).toFixed(0)}
                </Text>
            </View>
        );
    }
    renderMenu() {
        return (
            <View style={{ flexDirection: 'row' }}>

                <View style={{ flex: 1, alignItems: 'flex-end', marginRight: 10 }}>
                    <TouchableOpacity onPress={() =>
                        this.state.resizeMode === 'contain' ? this.setState({ resizeMode: 'cover' }) :
                            this.state.resizeMode === 'cover' ? this.setState({ resizeMode: 'stretch' }) :
                                this.state.resizeMode === 'stretch' ? this.setState({ resizeMode: 'contain' }) : this.setState({ resizeMode: 'contain' })}>
                        <Image style={{ width: 30, height: 30 }} source={ICON_RESIZE} resizeMode='contain' />
                    </TouchableOpacity>

                </View>
            </View >
        );
    }
    renderEspisode(data) {
        return (

            <View style={{ flex: 1 }}>
                <TouchableOpacity style={{ borderWidth: 1, padding: 4 }} activeOpacity={0.8}
                    onPress={() => this.getmovie(data.href)} disabled={this.state.flagLoad ? false : true}>
                    <Text style={{ color: "#ffffff", fontWeight: 'bold' }}>{data.serverName} - {data.title}{this.state.urlruning === data.href ? ' - Running' : ''}</Text>
                </TouchableOpacity>

            </View>
        );
    }
    renderChat() {
        return (
            <View style={{ flex: 1, justifyContent: 'flex-end' }}>

                <View style={{ height: height / 3, padding: 10 }}>

                    {this.state.datamessage.getRowCount() <= 0 ?
                        <View />
                        :
                        <ListView
                            dataSource={this.state.datamessage}
                            removeClippedSubviews={false}

                            ref={ref => this.listView = ref}
                            onLayout={event => {
                                this.listViewHeight = event.nativeEvent.layout.height
                            }}
                            onContentSizeChange={() => {
                                this.listView.scrollTo({ y: this.listView.getMetrics().contentLength - this.listViewHeight })
                            }}

                            renderRow={(data) => this.renderChatText(data)}
                        />

                    }
                </View>

                <View style={{ flexDirection: 'row', backgroundColor: 'transparent', borderRadius: 4,padding:5 }}>
                    <ScrollView style={{ maxHeight: 75 }}
                        ref={ref => this.scrollView = ref}
                        showsVerticalScrollIndicator={false}
                        onContentSizeChange={(contentWidth, contentHeight) => {
                            this.scrollView.scrollTo({ y: contentHeight, animated: true });
                        }}
                        >
                        <AutoGrowingTextInput
                            ref={(r) => { this._textInput = r; }}
                            placeholder={'Text Message!'}
                            placeholderTextColor={'gray'}
                            underlineColorAndroid={'transparent'}
                            style={{backgroundColor:'white'}}
                            numberOfLines = {4}
                            value={this.state.txtMessage}
                            onChangeText={(val) => {
                                this.setState({ txtMessage: val });

                            }}
                        />
                    </ScrollView>

                    <View style={{ justifyContent: 'flex-end' }}>
                        <TouchableOpacity
                            style={{
                                padding: 5, borderRadius: 4, backgroundColor: 'blue',
                                justifyContent: 'center', alignItems: 'center', height: 35
                            }}
                            onPress={() => {
                                this._textInput.resetHeightToMin();
                                this.sendMessage();
                            }}
                        >
                            <Text style={{ color: 'white', fontWeight: 'bold' }}>Send Message!</Text>
                        </TouchableOpacity>
                    </View>

                </View>

            </View>
        );
    }
    renderChatText(data) {
        return (
            <Text style={{ color: 'white', fontWeight: '500' }}>Admin : {data.txtMessage}</Text>
        );
    }

}

