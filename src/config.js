'use strict';

import {
    Dimensions,
    Platform
} from 'react-native';

const config = {

    STATUSBAR_COLOR: 'rgba(0, 0, 0, 0.6)',
    TABBAR_COLOR: 'transparent',
    TABBAR_BORDER_COLOR: 'transparent',
    API: 'https://moviepj.herokuapp.com',
};

export default config;