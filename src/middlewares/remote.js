'use strict';

import key from 'keymirror';

const typeSuffix = key({
    SUCCESS: null,
    FAIL: null
});

export const remoteMiddleware = store => next => action => {
    let remote = (url, method, body, callback) => {
        let request = {
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
        };

        if (method == 'POST') {
            request["method"] = method;
            request["body"] = JSON.stringify(body);
        }

        return fetch(url, request).then(response => response.json()).then(value => resolved({ store, next, action, value, callback }));
    };

    if (action.payload && action.payload.remote && action.payload.api) {
        let url = action.payload.remote + action.payload.api;
        return remote(url, action.payload.method, action.payload.body).catch(error => {
            console.log('Loi roi : ' + error);
        });
    }
    return next(action);
};

let resolved = (res) => {
    if (!res.value || res.value.error) {
        return res.next({
            type: res.action.type + "_" + typeSuffix.FAIL,
            payload: {
                error: res.value.error
            }
        });
    }
    if (res.callback) {
        res.callback(res.value.data);
    }
    return res.next({
        type: res.action.type + "_" + typeSuffix.SUCCESS,
        payload: res.value,
        params: res.action.params
    });
}

