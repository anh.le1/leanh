'use strict';

import {
    combineReducers
} from 'redux';

import router from './router/reducer';
import app from './app/reducer';
import user from './user/reducer';

const rootReducer = combineReducers({
    router,
    app,
    user
});

export default rootReducer;
