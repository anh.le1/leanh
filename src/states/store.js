'use strict';

import {
    createStore,
    applyMiddleware,
    compose
} from 'redux';

import thunk from 'redux-thunk';
import createLogger from 'redux-logger';
import reducers from './reducers';
import { storageMiddleware, promiseMiddleware } from '../middlewares';

const logger = createLogger();
const finalCreateStore = compose(
    applyMiddleware(thunk, promiseMiddleware, storageMiddleware)
)(createStore);
const store = finalCreateStore(reducers);

export default store;
