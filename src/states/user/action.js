'use strict';

import types from './types';

class Actions {

    static setDeviceInfo(data) {
        return {
            type: types.SET_DEVICE_INFO,
            payload: data
        }
    }

    static getLanguage() {
        return {
            type: types.GET_LANGUAGE,
            payload: {
                storage: {
                    key: "LANGUAGE",
                    type: "getInt",
                }
            }
        }
    }

    static setLanguage(data) {
        return {
            type: types.SET_LANGUAGE,
            payload: {
                storage: {
                    key: "LANGUAGE",
                    type: "setInt",
                    data: data
                }
            }
        }
    }

    // static getAsk() {
    //     return {
    //         type: types.GET_ASK,
    //         payload: {
    //             storage: {
    //                 key: "ASK",
    //                 type: "getString",
    //                 default: "true"
    //             }
    //         }
    //     }
    // }

    // static setAsk(data) {
    //     return {
    //         type: types.SET_ASK,
    //         payload: {
    //             storage: {
    //                 key: "ASK",
    //                 type: "setString",
    //                 data: data
    //             }
    //         }
    //     }
    // }

    static getHistory() {
        return {
            type: types.GET_HISTORY,
            payload: {
                storage: {
                    key: 'HISTORY',
                    type: 'getArrayObject'

                }
            }
        }
    }

    static setHistory(data) {
        return {
            type: types.SET_HISTORY,
            payload: {
                storage: {
                    key: 'HISTORY',
                    type: 'setArrayObject',
                    data: data
                }
            }
        }
    }
}

export function setDeviceInfo(data) {
    return next => next(Actions.setDeviceInfo(data));
}

export function getLanguage() {
    return next => next(Actions.getLanguage());
}

export function setLanguage(data) {
    return next => next(Actions.setLanguage(data));
}

// export function getAsk() {
//     return next => next(Actions.getAsk());
// }
// export function setAsk(data) {
//     return next => next(Actions.setAsk(data));
// }

export function getHistory() {
    return next => next(Actions.getHistory());
}
export function setHistory(data) {
    return next => next(Actions.setHistory(data));
}
