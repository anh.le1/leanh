'use strict';

import { Record } from 'immutable';

const InitialState = Record({
    deviceInfo: {},
    languageIndex: 2,
    // ask: 'true',
    history: []
});

export default InitialState;
