'use strict';

import InitialState from './';
import types from './types';
import createReducer from '../reducer';

const initialState = new InitialState();

export default createReducer(initialState, {
    [types.SET_DEVICE_INFO]: (state, data) => state.set('deviceInfo', data),

    [types.GET_LANGUAGE_SUCCESS]: (state, data) => state.set('languageIndex', data),
    [types.SET_LANGUAGE_SUCCESS]: (state, data) => state.set('languageIndex', data),

    // [types.GET_ASK_SUCCESS]: (state, data) => state.set('ask', data),
    // [types.SET_ASK_SUCCESS]: (state, data) => state.set('ask', data),

    [types.GET_HISTORY_SUCCESS]: (state, data) => state.set('history', data),
    [types.SET_HISTORY_SUCCESS]: (state, data) => state.set('history', data),
});
