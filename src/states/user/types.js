'use strict';

import key from 'keymirror';

export default key({
    SET_DEVICE_INFO: null,

    GET_LANGUAGE_SUCCESS: null,
    GET_LANGUAGE_FAIL: null,
    GET_LANGUAGE: null,

    SET_LANGUAGE_SUCCESS: null,
    SET_LANGUAGE_FAIL: null,
    SET_LANGUAGE: null,

    // GET_ASK: null,
    // GET_ASK_SUCCESS: null,
    // GET_ASK_FAIL: null,
    
    // SET_ASK: null,
    // SET_ASK_SUCCESS: null,
    // SET_ASK_FAIL: null,

    GET_HISTORY: null,
    GET_HISTORY_SUCCESS: null,
    GET_HISTORY_FAIL: null,

    SET_HISTORY: null,
    SET_HISTORY_SUCCESS: null,
    SET_HISTORY_FAIL: null,
});
