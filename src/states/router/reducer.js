'use strict';

import InitialState from './';

import {
    ActionConst
} from 'react-native-router-flux';
import createReducer from '../reducer';

const initialState = new InitialState();

export default createReducer(initialState, {
    [ActionConst.FOCUS]: (state, data, params, action) => state.set('scene', action.scene)
});
