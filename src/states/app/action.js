'use strict';

import types from './types';
import Configs from '../../config';
import Remote from '../../utils/remote';

class Actions {
    static setAndroidVersion(data) {
        return {
            type: types.SET_ANDROID_VERSION,
            payload: data
        }
    }

    //new  

    static getTopTrailer() {
        let url = Configs.API + '/topphimtrailer';
        console.log('url:', url);
        return {
            type: types.GET_TOPTRAILER,
            payload: {
                promise: Remote.request(url)
            }
        }
    }
    static getTopMovie() {
        let url = Configs.API + '/topphimle';
        console.log('url:', url);
        return {
            type: types.GET_TOPMOVIE,
            payload: {
                promise: Remote.request(url)
            }
        }
    }

    static getTopMovieSession() {
        let url = Configs.API + '/topphimbo';
        console.log('url:', url);
        return {
            type: types.GET_TOPMOVIESESSION,
            payload: {
                promise: Remote.request(url)
            }
        }
    }

    static getTrailer(page) {
        page ? page = page : page = 1;
        let url = Configs.API + '/trailer/' + page;
        console.log('url:', url);
        return {
            type: types.GET_TRAILER,
            payload: {
                promise: Remote.request(url)
            }
        }
    }
    static getMovie(page) {
        page ? page = page : page = 1;
        let url = Configs.API + '/phimle/' + page;
        console.log('url:', url);
        return {
            type: types.GET_MOVIE,
            payload: {
                promise: Remote.request(url)
            }
        }
    }

    static getMovieSession(page) {
        page ? page = page : page = 1;
        let url = Configs.API + '/phimbo/' + page;
        console.log('url:', url);
        return {
            type: types.GET_MOVIESESSION,
            payload: {
                promise: Remote.request(url)
            }
        }
    }

    static getMovieDetails(id) {
        id ? id = id : id = 'phim/bo-ba-sieu-viet-5095';
        let url = Configs.API + '/movieDetail/' + id;
        console.log('url:', url);
        return {
            type: types.GET_MOVIEDETAILS,
            payload: {
                promise: Remote.request(url)
            }
        }
    }

static geturlMovie(id) {
        id ? id = id : id = 'phim/bo-ba-sieu-viet-5095';
        let url = Configs.API + '/movieVideo/' + id;
        console.log('url:', url);
        return {
            type: types.GET_URLMOVIE,
            payload: {
                promise: Remote.request(url)
            }
        }
    }
static getespisode(id) {
        id ? id = id : id = 'phim/bo-ba-sieu-viet-5095';
        let url = Configs.API + '/getserver/' + id;
        console.log('url:', url);
        return {
            type: types.GET_LISTESPISODE,
            payload: {
                promise: Remote.request(url)
            }
        }
    }

static getsearchtext(id) {
        id ? id = id : id = '';
        let url = Configs.API + '/searchtext/' + id;
        console.log('url:', url);
        return {
            type: types.GET_LISTSEARCH,
            payload: {
                promise: Remote.request(url)
            }
        }
    }
    static getsearchadv(data1,data2,data3,data4) {
        // id ? id = id : id = '';
        let url = Configs.API + '/searchadv/' + data1+'/'+ data2+'/'+ data3+'/'+ data4;
        console.log('url:', url);
        return {
            type: types.GET_LISTSEARCHADV,
            payload: {
                promise: Remote.request(url)
            }
        }
    }
}


//new 


export function getTopTrailer() {
    return next => next(Actions.getTopTrailer());
}
export function getTopMovie() {
    return next => next(Actions.getTopMovie());
}
export function getTopMovieSession() {
    return next => next(Actions.getTopMovieSession());
}

export function getTrailer(page) {
    return next => next(Actions.getTrailer(page));
}
export function getMovie(page) {
    return next => next(Actions.getMovie(page));
}
export function getMovieSession(page) {
    return next => next(Actions.getMovieSession(page));
}

export function getMovieDetails(page) {
    return next => next(Actions.getMovieDetails(page));
}

export function geturlMovie(page) {
    return next => next(Actions.geturlMovie(page));
}

export function getespisode(page) {
    return next => next(Actions.getespisode(page));
}

export function getsearchtext(page) {
    return next => next(Actions.getsearchtext(page));
}
export function getsearchadv(data1,data2,data3,data4) {
    return next => next(Actions.getsearchadv(data1,data2,data3,data4));
}