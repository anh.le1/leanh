'use strict';

import { Record } from 'immutable';


const InitialState = Record({
      androidVersion: 21,
      //new
      listTopTrailer:{},
      listTopMovie:{},
      listTopMovieSession:{},

      listTrailer:{},
      listMovie:{},
      listMovieSession:{},

      movieDetails:{},
      urlMovie:{},
      listespisode:{},

      listSearch:{}
});

export default InitialState;