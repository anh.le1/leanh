import InitialState from './';
import types from './types';
import createReducer from '../reducer';
const initialState = new InitialState();

export default createReducer(initialState, {

    [types.SET_ANDROID_VERSION]: (state, data) => state.set('androidVersion', data),
    

    //new 

    [types.GET_TOPTRAILER_SUCCESS]: (state, data) => {
        return state.set('listTopTrailer', data)
    },
    [types.GET_TOPMOVIE_SUCCESS]: (state, data) => {
        return state.set('listTopMovie', data)
    },
    [types.GET_TOPMOVIESESSION_SUCCESS]: (state, data) => {
        return state.set('listTopMovieSession', data)
    },

    [types.GET_TRAILER_SUCCESS]: (state, data) => {
        return state.set('listTrailer', data)
    },
    [types.GET_MOVIE_SUCCESS]: (state, data) => {
        return state.set('listMovie', data)
    },
    [types.GET_MOVIESESSION_SUCCESS]: (state, data) => {
        return state.set('listMovieSession', data)
    },
    [types.GET_MOVIEDETAILS_SUCCESS]: (state, data) => {
        return state.set('movieDetails', data)
    },

    [types.GET_URLMOVIE_SUCCESS]: (state, data) => {
        return state.set('urlMovie', data)
    },
    [types.GET_LISTESPISODE_SUCCESS]: (state, data) => {
        return state.set('listespisode', data)
    },

    [types.GET_LISTSEARCH_SUCCESS]: (state, data) => {
        return state.set('listSearch', data)
    },

    [types.GET_LISTSEARCHADV_SUCCESS]: (state, data) => {
        return state.set('listSearch', data)
    },
});