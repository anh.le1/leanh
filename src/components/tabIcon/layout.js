'use strict';

import React, {
    Component
} from 'react';

import {
    View,
    Text,
    Image,
} from 'react-native';

import styles from './style';
import strings from './localize';

// const IMG_BACKGROUNG = require('../../resources/icon/backgroundTab.jpg')
export default class Layout extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.backgroundtab} />
                <Image
                    style={{
                        width: this.icons[this.props.name][2],
                        height: this.icons[this.props.name][3],
                    }}
                    source={this.icons[this.props.name][this.props.selected ? 1 : 0]}
                    resizeMode='contain'
                />
                <Text style={this.props.selected ? styles.tabNameSelected : styles.tabName}>
                    {strings[this.props.name]}
                </Text>
                
            </View>
        );
    }
}