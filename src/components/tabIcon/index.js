'use strict';

import {
    bindActionCreators
} from 'redux';

import {
    connect
} from 'react-redux';

import {
    Actions
} from 'react-native-router-flux';

import * as actions from '../../states/actions';
import Layout from './layout';

const ICON_HOME = require('../../resources/icon/home.png');
const ICON_HOME_SE = require('../../resources/icon/homeac.png');

const ICON_NEW = require('../../resources/icon/New.png');
const ICON_NEW_SE = require('../../resources/icon/newac2.png');

const ICON_VIEW = require('../../resources/icon/setting_inactive@3x.png');
const ICON_VIEW_SE = require('../../resources/icon/setting_active@3x.png');

const ICON_SEARCH = require('../../resources/icon/search.png');
const ICON_SEARCH_SE = require('../../resources/icon/search_icon_blue@3x.png');

class TabIcon extends Layout {

    constructor(props) {
        super(props);
        this.icons = {
            "home": [
                ICON_HOME, ICON_HOME_SE, 26, 26
            ],
            "view": [
                ICON_VIEW, ICON_VIEW_SE, 29, 22
            ],
            "search": [
                ICON_SEARCH, ICON_SEARCH_SE, 29, 22
            ]
        }
    }
}

const stateToProps = (state) => ({ ...state });
const dispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
};
export default connect(stateToProps, dispatchToProps)(TabIcon);
