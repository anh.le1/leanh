import {
    StyleSheet,
    Dimensions
} from 'react-native';

const {width,height} = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        paddingTop: 4,
        alignItems: "center",
        justifyContent: 'center',
        width: width / 3,
        height: 50,
        backgroundColor: 'transparent',
        // borderLeftWidth: 1,
        // borderLeftColor: 'darkblue'
    },
    backgroundtab:{
        position:'absolute',
        top:0,left:0,right:0,bottom:0,
        backgroundColor:'black',
        opacity:0.8
    },
    tabName: {
        marginTop: 4,
        fontSize: 11,
        color: "#ffffff",

    },
    tabNameSelected: {
        marginTop: 4,
        fontSize: 11,
        color: "#ffffff",
        fontWeight: 'bold'
    }
});

export default styles;