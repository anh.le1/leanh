'use strict';

import React, {
    Component

} from 'react';

import {
    Platform,
    View    
} from 'react-native';

import {
    AdMobBanner
} from 'react-native-admob';

import Config from '../../config';

export default class extends Component {
    constructor(props) {
        super(props);
        this.bannerError = this.bannerError.bind(this);
    }

    bannerError(error) {
        console.log(error);
    }

    render() {
        if (this.props.ads && Config[this.props.ads]) {
            return (
                <View style={{ height: 50 }}>                
                    <AdMobBanner
                        bannerSize={Config[this.props.ads].adType[Platform.OS]}
                        adUnitID={Config[this.props.ads].adUnit[Platform.OS]}
                        // testDeviceID="EMULATOR"
                        didFailToReceiveAdWithError={this.bannerError} />
                </View>
            );
        }
    }
}
