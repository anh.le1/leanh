'use strict';

import React, {
    Component
} from 'react';

import ProgressBar from 'react-native-progress/Bar';

export default class extends Component {

    constructor(props) {
        super(props);
        this.color = this.props.color || "#FFFFFF";
    }

    render() {
        let progress = this.props.progress / 100;
        if (this.props.progress) {
            return (
                <ProgressBar progress={progress} color={this.color} />
            );
        }
        else return null;
    }
}
