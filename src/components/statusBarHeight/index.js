'use strict';

import {
    bindActionCreators
} from 'redux';

import {
    connect
} from 'react-redux';

import * as actions from '../../states/actions';
import Layout from './layout';

class StatusBarHeight extends Layout {
    constructor(props) {
        super(props);
    }
}

const stateToProps = (state) => ({...state });
const dispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
};
export default connect(stateToProps, dispatchToProps)(StatusBarHeight);
