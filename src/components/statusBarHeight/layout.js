'use strict';

import React, {
    Component
} from 'react';

import {
    View,
    Platform,
    Dimensions
} from 'react-native';

const StatusBarManager = require('NativeModules').StatusBarManager;
const width = Dimensions.get('window').width;
import Config from '../../config';

export default class Layout extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        if (this.props.app.androidVersion == 21) {
            if (Platform.OS == 'ios') {
                return (
                    <View style={{ height: 20, width: width, backgroundColor: this.props.colorIos ? this.props.colorIos : 'transparent' }} />
                );
            }
            else {
                return (
                    <View style={{ height: StatusBarManager.HEIGHT, width: width, backgroundColor: this.props.color ? this.props.color : 'transparent' }} />
                );
            }
        }
        else {
            return null;
        }
    }
}
