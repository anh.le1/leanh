'use strict';
import React, { Component } from 'react';


import {
    Provider
} from 'react-redux';

import Store from './states/store';
import Views from './views';


export default class extends Component {
    render() {
        return (
            <Provider store={Store} >
                <Views />
            </Provider>
        );
    }
}