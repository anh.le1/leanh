'use strict';
import {
    Dimensions
} from 'react-native';

import Configs from '../config';
const {width, height} = Dimensions.get('window');

export default class {
    //Width 
    // mobile
    static scaleWidth(size) {
        return width < Configs.DEFAULT_WIDTH ? width * size / Configs.DEFAULT_WIDTH : size;
    }
    //mobile
    static scaleWidthToInt(size) {
        return width < Configs.DEFAULT_WIDTH ? parseInt(width * size / Configs.DEFAULT_WIDTH) : size;
    }
    // moblie & ipad 
    static scaleWidthAll(size) {
        return parseInt(width * size / Configs.DEFAULT_WIDTH);
    }

    //Height 
    // mobile
    static scaleHeight(size) {
        return height < Configs.DEFAULT_HEIGHT ? height * size / Configs.DEFAULT_HEIGHT : size;
    }
    // mobile
    static scaleHeightToInt(size) {
        return height < Configs.DEFAULT_HEIGHT ? parseInt(height * size / Configs.DEFAULT_HEIGHT) : size;
    }
    // moblie & ipad 
    static scaleHeightAll(size) {
        return height * size / Configs.DEFAULT_HEIGHT;
    }


} 