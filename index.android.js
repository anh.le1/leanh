'use strict';

import { AppRegistry } from 'react-native';

import src from './src';


AppRegistry.registerComponent('movie', () => src);
